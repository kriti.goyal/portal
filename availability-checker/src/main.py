from pymongo import MongoClient, ReplaceOne
from requests import get, ConnectionError, Timeout
from datetime import datetime
from os import environ
from time import sleep

def checkAvailability(entrypoint, retries=3, delay=10):
    for i in range(retries):
        lastResp = None
        try:
            r = get(entrypoint, timeout=5)
            if r.status_code == 200:
                return r.status_code, ""
            lastResp = r.status_code, ""
        except ConnectionError as ex:
            lastResp = -1, ex
        except Timeout as ex:
            lastResp = -2, ex
        sleep(delay)
    return lastResp

def getServices(collection):
    services = []
    for service in collection.find():
        services.append({key: service[key] for key in service.keys() & {'_id', 'entryPoint'}})
    
    return services

def updateDb(collection, services):
    updates = []
    for service in services:
        updates.append(ReplaceOne({"_id": service["_id"]}, service, upsert=True))
    collection.bulk_write(updates)

if __name__ == "__main__":
    host = environ.get("MONGODB_HOST", 'localhost')
    port = int(environ.get("MONGODB_PORT", "27017"))

    database = environ.get("MONGODB_DATABASE", "local")

    username = environ.get("MONGODB_USERNAME")
    password = environ.get("MONGODB_PASSWORD")
    authSource = environ.get("MONGODB_AUTHDB")

    if username:
        client = MongoClient(host=host, port=port, username=username, password=password, authSource=authSource)
    else:
        client = MongoClient(host=host, port=port)

    db = client.get_database(database)

    services = getServices(db.marketService)

    print(services)

    for service in services:
        status, errorMsg = checkAvailability(service['entryPoint'])
        service['status'] = status
        service['errorMsg'] = errorMsg
        service['lastUpdated'] = datetime.now()
        del service['entryPoint']
        print(service)
    
    updateDb(db.availability, services)
