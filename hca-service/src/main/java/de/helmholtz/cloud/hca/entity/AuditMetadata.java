package de.helmholtz.cloud.hca.entity;

import java.util.Set;
import java.util.TreeSet;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PROTECTED)
public class AuditMetadata {
    @CreatedDate
    private long createdDate;

    @LastModifiedDate
    private long lastModifiedDate;

    private Set<String> foreignKeys = new TreeSet<>();

    public void addForeignKey(String key) {
        foreignKeys.add(key);
    }

    public void removeForeignKey(String key) {
        foreignKeys.remove(key);
    }
}
