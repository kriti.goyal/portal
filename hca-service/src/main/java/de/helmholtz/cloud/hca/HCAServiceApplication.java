package de.helmholtz.cloud.hca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HCAServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HCAServiceApplication.class, args);
	}

}
