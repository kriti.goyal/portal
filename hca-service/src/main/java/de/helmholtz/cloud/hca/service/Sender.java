package de.helmholtz.cloud.hca.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import de.helmholtz.cloud.hca.entity.HCARequest;
import de.helmholtz.cloud.hca.repository.HCARequestRepository;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Sender {
    @Autowired
    private HCARequestRepository repository;
    private final RabbitTemplate rabbitTemplate;
    @Value("${hca.sender.queue}")
    private String queue;
    @Value("${hca.service}")
    private String serviceName;

    public Sender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelayString = "${hca.sender.delay}")
    public void send() {
        try {
            log.info("Check for new messages to send");
            ObjectMapper Obj = new ObjectMapper();

            List<HCARequest> requests = repository.findByStatusAndServiceName("created", this.serviceName);

            log.debug(String.format("Found %d messages ready to send", requests.size()));
            for (HCARequest request : requests) {
                MessageProperties props = MessagePropertiesBuilder.newInstance().setAppId(request.getServiceName())
                        .setType("ResourceAllocateV1").build();
                String payload = Obj.writeValueAsString(request.getRequest());
                props.setCorrelationId(request.getRequestId());

                Message jsonMessage = MessageBuilder.withBody(payload.getBytes()).andProperties(props).build();

                rabbitTemplate.send(this.queue, jsonMessage);
                log.debug("Sent message: " + payload);
                request.setStatus("sent");
                repository.save(request);
            }
        } catch (IOException e) {
            log.error("Cannot send messages " + e.getStackTrace());
        }
    }
}
