package de.helmholtz.cloud.hca.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.helmholtz.cloud.hca.entity.HCARequest;

public interface HCARequestRepository extends MongoRepository<HCARequest, String> {
    public List<HCARequest> findAll();
    public List<HCARequest> findByUserId(String userId);
    public List<HCARequest> findByRequestId(String requestId);
    public List<HCARequest> findByStatus(String status);
    public List<HCARequest> findByStatusAndServiceName(String status, String serviceName);
}