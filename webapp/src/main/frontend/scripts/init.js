function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}
function initRouter() {
    const outlet = document.querySelector('#outlet');
    let child;
    var pathname = window.location.pathname;

    if (pathname === '/') {
        import('../src/helmholtz-marketplace-app').then(async () => {
            removeAllChildNodes(outlet);
                await import('../src/views/landing-view/landing-view').then(() => {
                    child = document.createElement('helmholtz-marketplace-app');
                    child.appendChild(document.createElement('landing-view'));
                });
            outlet.appendChild(child);
        });
    } else if (pathname === '/services') {
        import('../src/helmholtz-marketplace-app').then(async () => {
            removeAllChildNodes(outlet);
            await import('../src/views/services-view/services-view').then(() => {
                child = document.createElement('helmholtz-marketplace-app');
                child.appendChild(document.createElement('services-view'));
                });
            outlet.appendChild(child);
        });
    } else if (pathname === '/forms') {
        import('../src/helmholtz-marketplace-app').then(async () => {
            await import('../src/views/forms/forms-view').then(() => {
                child = document.createElement('helmholtz-marketplace-app');
                child.appendChild(document.createElement('forms-view'));
                removeAllChildNodes(outlet);
                outlet.appendChild(child);
            });
        });
    };
}

window.addEventListener('load', () => {
    initRouter();
    document.getElementById('loading-overlay').hidden = true;
    document.body.classList.remove('hidden-overflow');
});

window.addEventListener('authentication-status-checker', () => {
    if (sessionStorage.getItem('auth_status') === 'authenticated') {
        if (!sessionStorage.getItem('reload_1')) {
            sessionStorage.setItem('reload_1', 'ok');
            window.location.reload();
        } else {
            sessionStorage.removeItem('reload_1');
        }
    }
});