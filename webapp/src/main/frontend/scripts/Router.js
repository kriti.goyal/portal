class Router {
    routes = [];

    mode = 'hash';

    root = '/';

    constructor(options) {
        this.mode = window.history.pushState ? 'history' : 'hash';
        if (options.mode) this.mode = options.mode;
        if (options.root) this.root = options.root;
        window.addEventListener('popstate', () => {
            this.render();
        });
    }

    add = (path, cb) => {
        this.routes.push({ path, cb });
        return this;
    };

    remove = path => {
        const { length } = this.routes;
        for (let i = 0; i < length; i += 1) {
            if (this.routes[i].path === path) {
                this.routes.slice(i, 1);
                return this;
            }
        }
        return this;
    };

    flush = () => {
        this.routes = [];
        return this;
    };

    clearSlashes = path => {
        if (path.endsWith('/')) {
            return path.substring(0, path.length - 1) === ''
                ? '/'
                : path.replace(/\/$/, '');
        }
        return path.startsWith('/') ? path : `/${path}`;
    };

    getFragment = () => {
        let fragment;
        if (this.mode === 'history') {
            fragment = this.clearSlashes(
                decodeURI(window.location.pathname + window.location.search)
            );
            fragment = fragment.replace(/\?(.*)$/, '');
            fragment = this.root !== '/' ? fragment.replace(this.root, '') : fragment;
        } else {
            const match = window.location.href.match(/#(.*)$/);
            fragment = match ? match[1] : '';
        }
        return this.clearSlashes(fragment);
    };

    navigate = (path = '') => {
        if (this.mode === 'history') {
            window.history.pushState(
                null,
                null,
                this.root + this.clearSlashes(path)
            );
        } else {
            window.location.href = `${window.location.href.replace(/#(.*)$/,'')}#${path}`;
        }
        this.render();
        return this;
    };

    render = () => {
        if (this.current === this.getFragment()) return;
        this.current = this.getFragment();

        const pathname = this.current.includes('?')
            ? this.clearSlashes(this.current.substring(0, this.current.indexOf('?')))
            : this.current;

        this.routes.some(route => {
            if (pathname === route.path) {
                route.cb.apply({}, [pathname]);
                return [pathname];
            }
            return false;
        });
    };
}
export default Router;
