import { LitElement, html, css } from 'lit-element';

import '../../common-components/service-card/service-card';

class ServicesList extends LitElement
{
    constructor()
    {
        super();
        this.services = [];
    }

    static get properties()
    {
        return {
            services: {
                type: Array
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: grid;
                padding-bottom: 20px;
                width: 100%;
                grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
                grid-gap: 2em;
                gap: 2em;
            }
        `;
    }
    render()
    {
        if (this.services == null) { return; }
        return html`
            <div class="container">
                ${this.services.map(service =>
                    html`<service-card  .service=${service}></service-card>`)}
            </div>
        `;
    }
}
customElements.define('services-list', ServicesList)