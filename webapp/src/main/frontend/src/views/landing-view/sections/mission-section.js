/* eslint class-methods-use-this: ["error", { "exceptMethods": ["_openMission"] }] */
import { LitElement, html, css } from 'lit-element';

import '@material/mwc-button';

class MissionSection extends LitElement
{
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                color: #5a5a5a;
            }
            .inner-section {
                display: flex;
                width: 100%;
                box-sizing: border-box;
                padding: 5%;
                flex-direction: column;
            }
            h1, h2, .statement, .button-container {
                width: 100%;
            }
            h1 {
                text-align: center;
                width: 100%;
                font-family: 'Hermann';
            }
            h2 {
                margin: auto;
                width: 90%;
                color: #6c757d;
                text-align: center;
                font-size: 1.8em;
                font-weight: 400;
                letter-spacing: -0.05rem;
                align-self: center;
            }
            .statement p {
                margin-right: 0.75rem;
                margin-left: 0.75rem;
                margin-bottom: 1rem;
                text-align: center;
                font-weight: 300;
                text-size-adjust: 100%;
                font-size: 1.2rem;
                line-height: 2;
            }
            .button-container {
                display: flex;
                justify-content: center;
            }
            .button-container mwc-button {
                --mdc-theme-primary: rgb(0,90,160);
                --mdc-theme-on-primary: white;
                width: 200px;
            }
            
            @media screen and (min-width: 1050px) {
                .inner-section {
                    width: 1000px;
                    margin-right: auto;
                    margin-left: auto;
                    padding: 5% 0;
                }
            }
        `;
    }

    render()
    {
        return html`
            <div class="inner-section">
                <h1 class="topic">Helmholtz Cloud Mission</h1>
                <h2 class="sub-topic">
                    Our mission is to strengthen and develop the ICT 
                    competences of the whole Helmholtz Association
                </h2>
                <div class="statement">
                    <p>
                        We want to support you - as a scientist, Helmholtz employee 
                        or stakeholder of research projects.
                        As a part of HIFIS, the Helmholtz Cloud will appear as a federated cloud.
                        All services are hosted by one of the 18 Helmholtz centres.
                    </p>
                </div>
                <div class="button-container">
                    <mwc-button unelevated @click="${this._openMission}">read more</mwc-button>
                </div>
            </div>
        `;
    }

    _openMission()
    {
        window.location.href = 'https://hifis.net/faq';
    }
}
customElements.define('mission-section', MissionSection);
