import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon-button';
import '@material/mwc-icon';

import '../../common-components/services-list/services-list.js';
import '../../common-components/service-profile/service-profile.js';
import '../../common-components/notification-banner/notification-banner.js';
import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';

class ServicesView extends LitElement {
    constructor() {
        super();
        this.addEventListener('helmholtz-cloud-service-metadata', this._showDetailsPanel);
        this.addEventListener(
            'helmholtz-cloud-dismiss-notification-services',
            this._notificationDismissalListener, true);
        const headers = new Headers({
            "Accept": "application/json"
        });
        if (sessionStorage.getItem('auth_status')) {
            headers.append("Authorization", `Bearer ${sessionStorage.getItem("access_token")}`);
        }
        this.serviceDetails = "";
        this.selectedProvider = null;

        var url = `${window.location.origin}${cp_prefix}api/v0/services?sort=software.asc`

        fetch(url, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(this._setup.bind(this))
            .catch(e => console.log(e));
    }
    static get properties() {
        return {
            services: {
                type: Array
            },
            serviceName: {
                type: String
            },
            serviceContact: {
                type: String
            }
        };
    }
    static get styles() {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            .header-border {
                border-bottom: 1px solid #274C69;
            }
            * {
                box-sizing: border-box;
            }
            notification-banner {
                background-color: #e3e3e3;
            }
            .list, .details {
                height: 100%;
            }
            main {
                display: flex;
                flex-direction: row;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100vh;
                overflow: hidden;
                padding-bottom: 30px;
            }
            /* Hide scrollbar for Chrome, Safari and Opera */
            .search::-webkit-scrollbar {
                display: none;
            }
            .search {
                display: flex;
                padding-top: 15px;
                padding-left: 20px;
                flex-direction: row;
                width: 100%;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
            }
            .content {
                display: flex;
                flex-direction: column;
                width: 100%;
            }
            /* Hide scrollbar for Chrome, Safari and Opera */
            .services::-webkit-scrollbar {
                display: none;
            }
            .services {
                display: flex;
                padding: 20px;
                flex-direction: row;
                width: 100%;
                height: 100%;
                overflow-y: scroll;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
            }
            .details {
                overflow: hidden;
                background-color: #efefef;
                width: 0;
                transition: width 0.3s linear;
            }
            #details.show {
                width: 600px;
            }
            #content.show {
                width: calc(100% - 600px);
            }
            .details .buttons {
                max-height: 50px;
                height: 5%;
                display: flex;
            }
            .buttons div {
                flex: 1 1 auto;
                display: flex;
                align-items: center;
                padding: 20px;
                color: #ff5722;
            }
            .buttons div a {
                text-decoration: none;
                font-size: 0.85em;
                padding-right: 5px;
                color: #ff5722;
                font-weight: 300;
            }
            .buttons div mwc-icon {
                font-size: 1.21em;
            }
            .details .details-content {
                background-color: white;
                margin: 20px;
                padding: 20px;
                border-radius: 3px;
                height: 90%;
                overflow-y: scroll;
            }
            footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
            .hide {
                display: none;
            }
            @media screen and (max-width: 771px) {
                #content.show {
                    display: none;
                }
                #details.show {
                    width: 100vw;
                }
            }
            @media screen and (min-width: 772px) {
                #content.show {
                    width: calc(100% - 390px);
                }
                #details.show {
                    width: 390px;
                }
            }
            @media screen and (min-width: 1100px) {
                #content.show {
                    width: calc(100% - 600px);
                }
                #details.show {
                    width: 600px;
                }
            }
        `;
    }
    render() {
        const notification = window.localStorage.getItem('notification-services');
        const msg = "Disclaimer: All Service information provided here is for your information and convenience. " +
            "The respective service provider is responsible for providing the service and all required information " +
            "on usage conditions and regulations. <br>" +
            "<p><b>Not all services are accessible to all users.</b></p>";
        return html`
                ${notification === "dismiss" ?
                html`
                        <header class="header-border">
                            <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
                        </header>`:
                html`
                        <header>
                            <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
                            <notification-banner message=${msg} page="services"></notification-banner>
                        </header>`}
            </header>
            <main>
                <div id="content" class="content">
                    <div class="search">
                        <form role="search">
                            <label for="servicesearch">Search Services:</label>
                            <input style='margin-left: 5px;' type="search" id="servicesearch" name="servicesearch">
                            <!--<label style='margin-left: 10px;' for="softwareselect">Software:</label>
                            <select style='margin-left: 5px;' name="softwareselect" id="softwareselect">
                            </select>-->
                            <label style='margin-left: 10px;' for="providerselect">Provider:</label>
                            <select style='margin-left: 5px;' name="providerselect" id="providerselect">
                            </select>
                            <label style='margin-left: 10px;' for="servicesort">Sort by:</label>
                            <select style='margin-left: 5px;' name="servicesort" id="servicesort">
                                <option value="software">Service Software</option>
                                <option value="name">Service Name</option>
                                <option value="providerName">Service Provider</option>
                            </select>
                            <button style='margin-left: 10px;' @click="${this._clear}" type="button" id="clearbutton">Reset all filters</button>
                        </form>
                    </div>
                    <div id="services" class="services">
                            <services-list id="serviceslist" .services=${this.services}></services-list>
                    </div>
                </div>
                <div id="details" class="details">
                    <div class=buttons>
                        <div>
                            <a href="mailto:${this.serviceContact}">
                                need help with ${this.serviceName}</a><mwc-icon>help</mwc-icon>
                        </div>
                        <mwc-icon-button icon="close" @click="${this._closeDetailsPanel}"></mwc-icon-button>
                    </div>
                    <div id="panel" class="details-content"></div>
                </div>
            </main>
            <footer>
                <helmholtz-cloud-footer minima></helmholtz-cloud-footer>
            </footer>
        `;
    }
    /* runs once after the first render and sets up the listeners for the different search/filter/sort inputs */
    firstUpdated() {
        super.firstUpdated();

        this._loadDropdownOptions();

        this.handleSearching = function (event) {
            this._closeDetailsPanel();
            var searchinput = this.shadowRoot.getElementById("servicesearch").value;
            var servicesort = this.shadowRoot.getElementById("servicesort").value;
            // var softwareselect = this.shadowRoot.getElementById("softwareselect").value;
            var providerselect = this.shadowRoot.getElementById("providerselect").value;

            this.newServices = this._deepCopy(this.services);
            if (searchinput.length != 0) {
                this.newServices = this._findServicesByPattern(this.newServices, searchinput.toLowerCase());
            }
            // if (!softwareselect.startsWith("--")) {
            //     this.newServices = this._selectServicesBySoftware(this.newServices, softwareselect);
            // }
            if (!providerselect.startsWith("--")) {
                this.newServices = this._selectServicesByProvider(this.newServices, providerselect);
            }
            this.newServices.sort((svc1, svc2) => {
                return this._compareObjects(svc1, svc2, servicesort);
            })
            var newList = document.createElement("services-list");
            newList.services = this.newServices;

            var servicesElement = this.shadowRoot.getElementById("services");
            servicesElement.innerHTML = "";
            servicesElement.appendChild(newList);
            this._updateSearchParams(false);
        }
        /* disable form submit and page reload on enter key */
        this.shadowRoot.getElementById("servicesearch").addEventListener('keydown', function (e) {
            if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
                e.preventDefault(); return false;
            }
        }, true);
        this.shadowRoot.getElementById("servicesearch").addEventListener("keyup", this.handleSearching.bind(this));
        this.shadowRoot.getElementById("servicesort").addEventListener("change", this.handleSearching.bind(this));
        // this.shadowRoot.getElementById("softwareselect").addEventListener("change", this.handleSearching.bind(this));
        this.shadowRoot.getElementById("providerselect").addEventListener("change", this.handleSearching.bind(this));
    }
    _setup(data) {
        this.services = data.content;

        this.services.forEach(service => {
            service.providerName = service.serviceProvider.nameDE;
            var softwares = [];
            service.softwareList.forEach(software => {
                softwares.push(software.name);
            });
            service.displaySoftware = softwares.join(", ");
        });
        this.dispatchEvent(
            new CustomEvent('helmholtz-cloud-custom-element-status', {
                detail: { customElementName: 'service-list' },
                bubbles: true,
                composed: true
            }));

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        /* check if any of the search/filter/sort options has been provided in the url params and apply them */
        const name = urlParams.get("search");
        const sort = urlParams.get("sort");
        // const software = urlParams.get("software");
        const provider = urlParams.get("provider");

        this.newServices = this._deepCopy(this.services);
        if (name != null) {
            this.shadowRoot.getElementById("servicesearch").value = name;
            this.newServices = this._findServicesByPattern(this.newServices, name.toLowerCase());
        }
        // if (software != null) {
        //     this.shadowRoot.getElementById("softwareselect").value = software;
        //     this.newServices = this._selectServicesBySoftware(this.newServices, software);
        // }
        if (provider != null) {
            this.selectedProvider = provider;
            // this.shadowRoot.getElementById("providerselect").value = provider;
            this.newServices = this._selectServicesByProvider(this.newServices, provider);
        }
        if (sort != null) {
            this.shadowRoot.getElementById("servicesort").value = sort;
            this.newServices.sort((svc1, svc2) => {
                return this._compareObjects(svc1, svc2, sort);
            })
        }
        var newList = document.createElement("services-list");
        newList.services = this.newServices;

        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);

        var imageurl = `${window.location.origin}${cp_prefix}api/v0/images?size=100`;
        fetch(imageurl)
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(this._resolveImages.bind(this));

        var availurl = `${window.location.origin}${cp_prefix}api/v0/availabilities?size=100`;
        fetch(availurl)
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(this._handleAvailability.bind(this));

        var serviceDetails = urlParams.get('serviceDetails');
        this.services.forEach(service => {
            if (service.name == serviceDetails) {
                this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-metadata', {
                    detail: { message: service }, bubbles: true, composed: true
                }));
            }
        });
    }
    _resolveImages(data) {
        let imageMap = {};
        for (const image of data.content) {
            const key = image.uuid;
            if (!(key in imageMap)) {
                imageMap[key] = image.image;
            }
        }
        this.services.forEach(service => {
            service.img = imageMap[service.logoId];
            service.providerImg = imageMap[service.serviceProvider.logoId];
        });

        this.newServices.forEach(service => {
            service.img = imageMap[service.logoId];
            service.providerImg = imageMap[service.serviceProvider.logoId];
        });
        var newList = document.createElement("services-list");
        newList.services = this.newServices;

        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
    }
    _handleAvailability(data) {
        let availMap = {};
        for (const avail of data.content) {
            const key = avail.id;
            if (!(key in availMap)) {
                availMap[key] = avail;
            }
        }
        this.services.forEach(service => {
            var avail = availMap[service.uuid];
            service.availability = {};
            var d = new Date(avail['lastUpdated']);
            service.availability['lastChecked'] = `${d.getDate()}.${d.getMonth() + 1}.${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
            if (avail['status'] == 200) {
                service.availability['status'] = "Available";
            } else if (avail['status'] < 0) {
                service.availability['status'] = "Not available";
                service.availability['reason'] = avail['errorMsg'];
            } else {
                service.availability['status'] = "Not available";
                service.availability['reason'] = HTTP_STATUS_CODES[`CODE_${avail['status']}`];
            }
        });
        this.newServices.forEach(service => {
            var avail = availMap[service.uuid];
            service.availability = {};
            var d = new Date(avail['lastUpdated']);
            service.availability['lastChecked'] = `${d.getDate()}.${d.getMonth() + 1}.${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
            if (avail['status'] == 200) {
                service.availability['status'] = "Available";
            } else if (avail['status'] < 0) {
                service.availability['status'] = "Not available";
                service.availability['reason'] = avail['errorMsg'];
            } else {
                service.availability['status'] = "Not available";
                service.availability['reason'] = HTTP_STATUS_CODES[`CODE_${avail['status']}`];
            }
        });
        var newList = document.createElement("services-list");
        newList.services = this.newServices;

        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
    }
    _deepCopy(array) {
        var newArray = []
        array.forEach(item => {
            newArray.push(JSON.parse(JSON.stringify(item)));
        })
        return newArray;
    }
    /* checks if the different inputs are different from default and updates the url query accordingly */
    _updateSearchParams() {
        var searchinput = this.shadowRoot.getElementById("servicesearch").value;
        var servicesort = this.shadowRoot.getElementById("servicesort").value;
        // var softwareselect = this.shadowRoot.getElementById("softwareselect").value;
        var providerselect = this.shadowRoot.getElementById("providerselect").value;

        var newQuery = ""
        if (searchinput.length != 0) {
            newQuery += `&search=${searchinput}`;
        }
        // if (!softwareselect.startsWith("--")) {
        //     newQuery += `&software=${softwareselect}`;
        // }
        if ((providerselect.length != 0) && (!providerselect.startsWith("--"))) {
            newQuery += `&provider=${providerselect}`;
        } else {
            if (this.selectedProvider) {

                newQuery += `&provider=${this.selectedProvider}`;
            }
        }
        if (servicesort != 'software') {
            newQuery += `&sort=${servicesort}`;
        }
        if (this.serviceDetails.length != 0) {
            newQuery += `&serviceDetails=${encodeURIComponent(this.serviceDetails)}`;
        }
        newQuery = "?" + newQuery.substring(1);

        if (newQuery == '?') newQuery = '';
        if (history.pushState) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + newQuery;
            window.history.pushState({ path: newurl }, '', newurl);
        }
    }
    /* helper methods to sort services */
    _compareObjects(object1, object2, key) {
        const obj1 = object1[key].toUpperCase()
        const obj2 = object2[key].toUpperCase()

        if (obj1 < obj2) {
            return -1
        }
        if (obj1 > obj2) {
            return 1
        }
        return 0
    }
    _highlightText(text, pattern) {
        const regex = new RegExp(pattern, 'gi');
        return text.replace(regex, "<span style='background-color: rgba(140,180,35,.2)'>$&</span>");
    }
    /* takes an array of services and checks the service name against the provided pattern */
    _findServicesByPattern(services, pattern) {
        var matchedServices = []
        services.forEach(service => {
            var found = false;
            if (service.name.includes(pattern)) {
                found = true;
            }
            if (service.displaySoftware.toLowerCase().includes(pattern)) {
                found = true;
            }
            if (service.summary.toLowerCase().includes(pattern)) {
                found = true;
                service.summary = this._highlightText(service.summary, pattern);
            }
            if (service.serviceProvider.abbreviation.toLowerCase().includes(pattern)) {
                found = true;
            }
            if (service.serviceProvider.nameDE.toLowerCase().includes(pattern)) {
                found = true;
            }
            if (service.description.toLowerCase().includes(pattern)) {
                found = true;
                service.foundDescription = true;
            }
            if (found) {
                matchedServices.push(service);
            }
        });

        return matchedServices;
    }
    /* takes an array of services and checks the service provider against the given input */
    _selectServicesByProvider(services, provider) {
        var matchedServices = [];
        services.forEach(service => {
            if (service.serviceProvider.abbreviation == provider) {
                matchedServices.push(service);
            }
        });
        return matchedServices;
    }
    /* takes an array of services and checks the service software against the given input */
    _selectServicesBySoftware(services, software) {
        var matchedServices = [];
        services.forEach(service => {
            var found = false;
            service.softwareList.forEach(sft => {
                if (sft.name == software) {
                    found = true;
                }
            });
            if (found) {
                matchedServices.push(service);
            }
        });
        return matchedServices;
    }
    /* gets the distinct values for software and providers from the backend and fills the dropdown menus */
    _loadDropdownOptions() {
        // var urlSoftware = `${window.location.origin}${cp_prefix}api/v0/services/uniqueEntries/softwareList,name`
        var urlProvider = `${window.location.origin}${cp_prefix}api/v0/services/uniqueEntries/serviceProvider,abbreviation`

        // fetch(urlSoftware)
        //     .then((response) => {
        //         if (response.status !== 200) {
        //             throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
        //         }
        //         return response.json();
        //     })
        //     .then(data => {
        //         const queryString = window.location.search;
        //         const urlParams = new URLSearchParams(queryString);
        //         const software = urlParams.get("software");
        //         var select = this.shadowRoot.getElementById('softwareselect');
        //         var first = document.createElement('option');
        //         first.innerHTML = ' -- select an option -- ';
        //         first.disabled = true;
        //         if (software == null) {
        //             first.selected = true;
        //         }
        //         select.appendChild(first);
        //         for (var i = 0; i < data.length; i++) {
        //             var opt = document.createElement('option');
        //             opt.value = data[i];
        //             opt.innerHTML = data[i];
        //             if (data[i] == software) {
        //                 opt.selected = true;
        //             }
        //             select.appendChild(opt);
        //         }
        //     })
        //     .catch(e => console.log(e));
        fetch(urlProvider)
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                var select = this.shadowRoot.getElementById('providerselect');
                const queryString = window.location.search;
                const urlParams = new URLSearchParams(queryString);
                const provider = urlParams.get("provider");
                var first = document.createElement('option');
                first.innerHTML = ' -- select an option -- ';
                first.disabled = true;
                if (provider == null) {
                    first.selected = true;
                }
                select.appendChild(first);
                for (var i = 0; i < data.length; i++) {
                    var opt = document.createElement('option');
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    if (data[i] == provider) {
                        opt.selected = true;
                    }
                    select.appendChild(opt);
                }
            })
            .catch(e => console.log(e));
    }
    /* clears all input and url parameters and sets the list of displayed services back to default */
    _clear(e) {
        this.shadowRoot.getElementById("servicesearch").value = "";
        this.shadowRoot.getElementById("servicesort").value = "software";
        // this.shadowRoot.getElementById("softwareselect").selectedIndex = 0;
        this.shadowRoot.getElementById("providerselect").selectedIndex = 0;
        this.services.sort((svc1, svc2) => {
            return this._compareObjects(svc1, svc2, "software");
        })
        var newList = document.createElement("services-list");
        newList.services = this._deepCopy(this.services);

        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
        this._closeDetailsPanel();
    }
    /* opens the details panel for a given service */
    _showDetailsPanel(e) {
        const el = this.shadowRoot.getElementById("panel");
        const newService = e.detail.message;
        const oldService = el.querySelector('service-profile') ?
            el.querySelector('service-profile').service : null;
        if (!oldService || (newService["uuid"] !== oldService["uuid"])) {
            this.serviceName = newService['displayName'];
            this.serviceContact = newService['email'];
            const serviceProfile = document.createElement('service-profile');
            serviceProfile.uuid = newService["uuid"];
            serviceProfile.service = newService["service"];
            this._removeAllChildNodes(el);
            el.appendChild(serviceProfile);
        }
        this.shadowRoot.getElementById('content').classList.add('show');
        this.shadowRoot.getElementById('details').classList.add('show');
        this.serviceDetails = newService.name;
        this._updateSearchParams();
    }
    /* closed the details panel */
    _closeDetailsPanel() {
        this.shadowRoot.getElementById('content').classList.remove('show');
        this.shadowRoot.getElementById('details').classList.remove('show');
        const el = this.shadowRoot.getElementById("panel");
        this._removeAllChildNodes(el);
        this.serviceDetails = "";
        this._updateSearchParams();
    }
    /* helper method to remove all childs nodes for a given parent */
    _removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }
    /* hides the notification banner if the user already dismissed it previously */
    _notificationDismissalListener() {
        this.shadowRoot.querySelector('notification-banner').classList.add('hide');
        this.shadowRoot.querySelector('header').classList.add('header-border');
    }
}
customElements.define('services-view', ServicesView);
