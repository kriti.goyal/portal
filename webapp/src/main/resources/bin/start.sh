#!/bin/bash

port=8080
currentDir=$(pwd)
parentDir="$(dirname "$currentDir")"

while getopts "a:d:f:p:s:" flag;
do
    case "${flag}" in
        a) profile=${OPTARG};;
        d) parentDir=${OPTARG};;
        f) filePath=${OPTARG};;
        p) port=${OPTARG};;
        s) secret=${OPTARG};;
        *) echo 'error' >&2
           exit 1
    esac
done

if [ ! "$secret" ]; then
    echo 'Option -s missing' >&2
    exit 1
fi

if [ ! "$filePath" ]; then
    filePath=file:$parentDir/webapp/
fi

if [ ! "$profile" ]; then
    java -Xms256m -Xmx512m -server -jar "$parentDir"/lib/webapp-0.0.2-SNAPSHOT.jar --spring.security.oauth2.client.registration.unity.client-secret=$secret --server.port=$port --spring.resources.static-locations="$filePath" --server.servlet.context-path="$CONTEXT_PATH"
    exit 1
fi

java -Xms256m -Xmx512m -Dspring.profiles.active="$profile" -server -jar "$parentDir"/lib/webapp-0.0.2-SNAPSHOT.jar --spring.security.oauth2.client.registration.unity.client-secret=$secret --server.port=$port --spring.resources.static-locations="$filePath" --server.servlet.context-path="$CONTEXT_PATH"

