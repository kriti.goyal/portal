package de.helmholtz.cloud.webappserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
public class HelmholtzMarketplaceServerSecurityConfig extends WebSecurityConfigurerAdapter
{
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        SimpleUrlAuthenticationFailureHandler handler = new SimpleUrlAuthenticationFailureHandler("/");

        // @formatter:off
        http
            .authorizeRequests(requests -> requests
                    .antMatchers("/**", "/tokens", "/error", "/actuator/**", "/favicon.ico").permitAll()
                    .anyRequest().authenticated()
            )
            .exceptionHandling().disable()
            .logout(l -> l
                    .logoutSuccessUrl("/").permitAll()
            )
            .oauth2Login(o -> o
                    .failureHandler((request, response, exception) -> {
                        request.getSession().setAttribute("error.message", exception.getMessage());
                        handler.onAuthenticationFailure(request, response, exception);
                    })
            );
        // @formatter:on
    }
}
