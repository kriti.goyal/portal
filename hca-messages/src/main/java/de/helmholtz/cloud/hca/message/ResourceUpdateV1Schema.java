package de.helmholtz.cloud.hca.message;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * ResourceUpdateV1
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "vos",
    "type",
    "specification",
    "requester"
})
@Generated("jsonschema2pojo")
public class ResourceUpdateV1Schema {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    private String id;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vos")
    private List<String> vos = new ArrayList<String>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private String type;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("specification")
    private UpdateResourceSpecification specification;
    /**
     * Requester
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("requester")
    private RequesterV1Schema requester;

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vos")
    public List<String> getVos() {
        return vos;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vos")
    public void setVos(List<String> vos) {
        this.vos = vos;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("specification")
    public UpdateResourceSpecification getSpecification() {
        return specification;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("specification")
    public void setSpecification(UpdateResourceSpecification specification) {
        this.specification = specification;
    }

    /**
     * Requester
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("requester")
    public RequesterV1Schema getRequester() {
        return requester;
    }

    /**
     * Requester
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("requester")
    public void setRequester(RequesterV1Schema requester) {
        this.requester = requester;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ResourceUpdateV1Schema.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("vos");
        sb.append('=');
        sb.append(((this.vos == null)?"<null>":this.vos));
        sb.append(',');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null)?"<null>":this.type));
        sb.append(',');
        sb.append("specification");
        sb.append('=');
        sb.append(((this.specification == null)?"<null>":this.specification));
        sb.append(',');
        sb.append("requester");
        sb.append('=');
        sb.append(((this.requester == null)?"<null>":this.requester));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.requester == null)? 0 :this.requester.hashCode()));
        result = ((result* 31)+((this.specification == null)? 0 :this.specification.hashCode()));
        result = ((result* 31)+((this.id == null)? 0 :this.id.hashCode()));
        result = ((result* 31)+((this.vos == null)? 0 :this.vos.hashCode()));
        result = ((result* 31)+((this.type == null)? 0 :this.type.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ResourceUpdateV1Schema) == false) {
            return false;
        }
        ResourceUpdateV1Schema rhs = ((ResourceUpdateV1Schema) other);
        return ((((((this.requester == rhs.requester)||((this.requester!= null)&&this.requester.equals(rhs.requester)))&&((this.specification == rhs.specification)||((this.specification!= null)&&this.specification.equals(rhs.specification))))&&((this.id == rhs.id)||((this.id!= null)&&this.id.equals(rhs.id))))&&((this.vos == rhs.vos)||((this.vos!= null)&&this.vos.equals(rhs.vos))))&&((this.type == rhs.type)||((this.type!= null)&&this.type.equals(rhs.type))));
    }

}
