package de.helmholtz.cloud.cerebrum.controller;

import java.util.List;

import javax.validation.constraints.Min;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.hca.message.ResourceAllocateV1Schema;
import de.helmholtz.cloud.cerebrum.entity.HCARequest;
import de.helmholtz.cloud.cerebrum.errorhandling.CerebrumApiError;
import de.helmholtz.cloud.cerebrum.service.HCARequestService;
import de.helmholtz.cloud.cerebrum.utils.CerebrumControllerUtilities;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.SneakyThrows;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "${spring.data.rest.base-path}/hca")
@Tag(name = "HCA", description = "API to send messages to the Cloud Agent")
public class HCAController {

    private final HCARequestService hcaRequestService;

    public HCAController(HCARequestService hcaRequestService) {
        this.hcaRequestService = hcaRequestService;
    }

    /* list requests for user */
    @Operation(summary = "get all requests for a user")
    @GetMapping(path = "/{userId}")
    public Iterable<HCARequest> getRequestsByUserId(
            @Parameter(description = "specify the user ID") @PathVariable(name = "userId") String userId,
            @Parameter(description = "specify the page number") @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
            @Parameter(description = "limit the number of records returned in one page") @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
            @Parameter(description = "sort the fetched data in either ascending (asc) "
                    + "or descending (desc) according to one or more of the images "
                    + "properties. Eg. to sort the list in ascending order base on the "
                    + "name property; the value will be set to name.asc") @RequestParam(value = "sort", defaultValue = "createdDate.desc") List<String> sorts) {

        return hcaRequestService.getHCARequestsByUserId(userId,
                PageRequest.of(page, size, Sort.by(CerebrumControllerUtilities.getOrders(sorts))));
    }

    /* list requests for user */
    @Operation(summary = "get requests for a user for a service")
    @GetMapping(path = "/{userId}/{serviceName}")
    public Iterable<HCARequest> getRequestsByUserIdAndServiceName(
            @Parameter(description = "specify the user ID") @PathVariable(name = "userId") String userId,
            @Parameter(description = "specify the service name") @PathVariable(name = "serviceName") String serviceName,
            @Parameter(description = "specify the page number") @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
            @Parameter(description = "limit the number of records returned in one page") @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
            @Parameter(description = "sort the fetched data in either ascending (asc) "
                    + "or descending (desc) according to one or more of the images "
                    + "properties. Eg. to sort the list in ascending order base on the "
                    + "name property; the value will be set to name.asc") @RequestParam(value = "sort", defaultValue = "createdDate.desc") List<String> sorts) {

        return hcaRequestService.getHCARequestsByUserIdAndServiceName(userId, serviceName,
                PageRequest.of(page, size, Sort.by(CerebrumControllerUtilities.getOrders(sorts))));
    }

    /* allocate resource */
    @SneakyThrows
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "submit a new resource allocation request", security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "resource allocation request created", content = @Content(schema = @Schema(implementation = HCARequest.class))),
            @ApiResponse(responseCode = "400", description = "invalid UUID supplied", content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content()),
            @ApiResponse(responseCode = "403", description = "forbidden", content = @Content()),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content())})
    @PostMapping(path = "/resource/{serviceName}")
    public ResponseEntity<HCARequest> allocateResource(
            @Parameter(description = "Unique identifier of the associated service") @PathVariable(name = "serviceName") String serviceName,
            @RequestBody ResourceAllocateV1Schema request, UriComponentsBuilder uriComponentsBuilder) {
        return hcaRequestService.createHCARequest(serviceName, request, uriComponentsBuilder);
    }
}
