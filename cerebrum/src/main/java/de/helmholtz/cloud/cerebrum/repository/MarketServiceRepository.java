package de.helmholtz.cloud.cerebrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

import de.helmholtz.cloud.cerebrum.entity.MarketService;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface MarketServiceRepository extends MongoRepository<MarketService, String>, CerebrumRepository<MarketService>
{
    Optional<MarketService> findByUuid(String uuid);

    Page<MarketService> findByName(@Param("name") String name, PageRequest pageRequest);

    Page<MarketService> findByEntryPoint(@Param("entryPoint") String url, PageRequest pageRequest);

    @Query("{'serviceProvider.nameDE' : ?0 }")
    Page<MarketService> findByServiceProviderNameDE(String nameDE, PageRequest pageRequest);

    @Query("{'serviceProvider.abbreviation' : ?0 }")
    Page<MarketService> findByServiceProviderAbbreviation(String abbreviation, PageRequest pageRequest);

    @Query(value = "{'softwareList.name' : ?0 }")
    Page<MarketService> findBySoftwareListName(String name, PageRequest pageRequest);

    @Query(value = "{$text: {$search: '?0'}}")
    Page<MarketService> searchByText(String text, PageRequest pageRequest);
    @Query(value = "{'softwareList.name': '?0'}")
    Page<MarketService> searchBySoftware(String software, PageRequest pageRequest);
    @Query(value = "{'serviceProvider.abbreviation': '?0'}")
    Page<MarketService> searchByProvider(String provider, PageRequest pageRequest);
    @Query(value = "{'softwareList.name': '?0', 'serviceProvider.abbreviation': '?1'}")
    Page<MarketService> searchBySoftwareAndProvider(String software, String provider, PageRequest pageRequest);
    @Query(value = "{$text: {$search: '?0'}, 'softwareList.name': '?1'}")
    Page<MarketService> searchByTextAndSoftware(String text, String software, PageRequest pageRequest);
    @Query(value = "{$text: {$search: '?0'}, 'serviceProvider.abbreviation': '?1'}")
    Page<MarketService> searchByTextAndProvider(String text, String provider, PageRequest pageRequest);
    @Query(value = "{$text: {$search: '?0'}, 'softwareList.name': '?1', 'serviceProvider.abbreviation': '?2'}")
    Page<MarketService> searchByTextAndSoftwareAndProvider(String text, String software, String provider, PageRequest pageRequest);

    @SuppressWarnings("UnusedReturnValue")
    Optional<MarketService> deleteByUuid(String id);
}