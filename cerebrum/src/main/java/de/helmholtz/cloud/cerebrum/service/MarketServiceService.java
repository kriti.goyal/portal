package de.helmholtz.cloud.cerebrum.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.FindIterable;

import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.cerebrum.entity.MarketService;
import de.helmholtz.cloud.cerebrum.repository.MarketServiceRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class MarketServiceService extends CerebrumServiceBase<MarketService, MarketServiceRepository, ForeignKeyExecutorService>
{
    private final MongoOperations mongoTemplate;
    private final MarketServiceRepository marketServiceRepository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    public MarketServiceService(MarketServiceRepository marketServiceRepository,
                                ForeignKeyExecutorService foreignKeyExecutorService,
                                MongoOperations mongoTemplate)
    {
        super(MarketService.class, MarketServiceRepository.class, ForeignKeyExecutorService.class);
        this.marketServiceRepository = marketServiceRepository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
        this.mongoTemplate = mongoTemplate;
    }

    public List<String> getUnique(String field) {
        List<String> result = new ArrayList<String>();
        DistinctIterable<String> tmp = mongoTemplate.getCollection("marketService").distinct(field.replace(',', '.'), String.class);
        for (String t: tmp) {
            result.add(t);
        }
        return result;
    }

    public Page<MarketService> getServices(PageRequest page)
    {
        return getAllEntities(page, marketServiceRepository);
    }

    public MarketService getService(String uuid)
    {
        return getEntity(uuid, marketServiceRepository);
    }

    public Page<MarketService> getServiceByAttributes(PageRequest page, String attr, String value)
    {
        return getEntities(page, attr, value, marketServiceRepository);
    }

    public Page<MarketService> searchServices(PageRequest page, String text, String software, String provider)
    {
        if (text != null) {
            if (software != null) {
                if (provider != null) {
                    return this.marketServiceRepository.searchByTextAndSoftwareAndProvider(text, software, provider, page);
                } else {
                    return this.marketServiceRepository.searchByTextAndSoftware(text, software, page);
                }
            } else {
                if (provider != null) {
                    return this.marketServiceRepository.searchByTextAndProvider(text, provider, page);
                } else {
                    return this.marketServiceRepository.searchByText(text, page);
                }
            }
        } else {
            if (software != null) {
                if (provider != null) {
                    return this.marketServiceRepository.searchBySoftwareAndProvider(software, provider, page);
                } else {
                    return this.marketServiceRepository.searchBySoftware(software, page);
                }
            } else {
                if (provider != null) {
                    return this.marketServiceRepository.searchByProvider(provider, page);
                }
            }
        }
        return this.marketServiceRepository.findAll(page);
    }

    public ResponseEntity<MarketService> createService(
            MarketService entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return createEntity(entity, marketServiceRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<MarketService> updateService(
            String uuid, MarketService entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return updateEntity(uuid, entity, marketServiceRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<MarketService> partiallyUpdateService(String uuid, JsonPatch patch)
    {
        return partiallyUpdateEntity(uuid, marketServiceRepository, foreignKeyExecutorService, patch);
    }

    public ResponseEntity<MarketService> deleteService(String uuid)
    {
        return deleteEntity(uuid, marketServiceRepository, foreignKeyExecutorService);
    }

    //Service Management Team
    public ResponseEntity<MarketService> addTeamMember(String serviceUuid, String personUuid)
    {
        return teamMember(serviceUuid, personUuid, true);
    }

    public ResponseEntity<MarketService> deleteTeamMember(String serviceUuid, String personUuid)
    {
        return teamMember(serviceUuid, personUuid, false);
    }

    @SneakyThrows
    private ResponseEntity<MarketService> teamMember(String serviceUuid, String personUuid, boolean toAdd)
    {
        MarketService retrievedService = getService(serviceUuid);

        ObjectMapper objectMapper = new ObjectMapper();
        MarketService submittedService = objectMapper
                .readValue(objectMapper.writeValueAsString(retrievedService), MarketService.class);

        if (toAdd) {
            if (retrievedService.getManagementTeam().contains(personUuid)) {
                return ResponseEntity.noContent().build();
            }
            submittedService.addTeamMember(personUuid);
        } else {
            if (!retrievedService.getManagementTeam().contains(personUuid)) {
                return ResponseEntity.noContent().build();
            }
            submittedService.removeTeamMember(personUuid);
        }

        foreignKeyExecutorService.executeBeforeUpdate(retrievedService, submittedService);
        MarketService updatedService = marketServiceRepository.save(submittedService);
        return ResponseEntity.ok().body(updatedService);
    }
}
