package de.helmholtz.cloud.cerebrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.hca.message.ResourceAllocateV1Schema;
import de.helmholtz.cloud.cerebrum.entity.HCARequest;
import de.helmholtz.cloud.cerebrum.repository.HCARequestRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;
import lombok.SneakyThrows;

@Service
public class HCARequestService
        extends CerebrumServiceBase<HCARequest, HCARequestRepository, ForeignKeyExecutorService> {
    private final HCARequestRepository repository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected HCARequestService(HCARequestRepository repository, ForeignKeyExecutorService foreignKeyExecutorService) {
        super(HCARequest.class, HCARequestRepository.class, ForeignKeyExecutorService.class);
        this.repository = repository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<HCARequest> getHCARequests(PageRequest page) {
        return getAllEntities(page, repository);
    }

    public Page<HCARequest> getHCARequestsByUserId(String userId, PageRequest page) {
        return repository.findByUserId(userId, page);
    }

    public Page<HCARequest> getHCARequestsByUserIdAndServiceName(String userId, String serviceName, PageRequest page) {
        return repository.findByUserIdAndServiceName(userId, serviceName, page);
    }

    public Page<HCARequest> getHCARequestsByRequestId(String requestId, PageRequest page) {
        return repository.findByRequestId(requestId, page);
    }

    public HCARequest getHCARequest(String uuid) {
        return getEntity(uuid, repository);
    }

    @SneakyThrows
    public ResponseEntity<HCARequest> createHCARequest(String serviceName, ResourceAllocateV1Schema request,
            UriComponentsBuilder uriComponentsBuilder) {
        HCARequest entity = new HCARequest(request, serviceName);

        return createEntity(entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    @SneakyThrows
    public ResponseEntity<HCARequest> updateHCARequest(String uuid, HCARequest entity,
            UriComponentsBuilder uriComponentsBuilder) {
        return updateEntity(uuid, entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<HCARequest> deleteHCARequest(String uuid) {
        return deleteEntity(uuid, repository, foreignKeyExecutorService);
    }
}
