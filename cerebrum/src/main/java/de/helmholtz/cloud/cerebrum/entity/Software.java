package de.helmholtz.cloud.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

import de.helmholtz.cloud.cerebrum.annotation.ForeignKey;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class Software
{
    @NotNull
    @Schema(description = "Name of a software", example = "NextCloud", required = true)
    private String name;

    private String description;

    @Schema(description = "", example = "img-01eac6d7-0d35-1812-a3ed-24aec4231940")
    @ForeignKey
    private String logoId;
}
