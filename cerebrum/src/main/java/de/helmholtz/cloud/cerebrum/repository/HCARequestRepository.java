package de.helmholtz.cloud.cerebrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import de.helmholtz.cloud.cerebrum.entity.HCARequest;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface HCARequestRepository extends MongoRepository<HCARequest, String>, CerebrumRepository<HCARequest> {
    Optional<HCARequest> findByUuid(String uuid);

    Optional<HCARequest> deleteByUuid(String uuid);

    Page<HCARequest> findByUserId(String userId, PageRequest page);

    Page<HCARequest> findByUserIdAndServiceName(String userId, String serviceName, PageRequest page);

    Page<HCARequest> findByRequestId(String requestId, PageRequest page);
}
