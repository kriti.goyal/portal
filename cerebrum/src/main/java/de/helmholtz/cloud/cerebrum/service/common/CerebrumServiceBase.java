package de.helmholtz.cloud.cerebrum.service.common;

import com.github.fge.jsonpatch.JsonPatch;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import de.helmholtz.cloud.cerebrum.errorhandling.exception.CerebrumEntityNotFoundException;
import de.helmholtz.cloud.cerebrum.utils.CerebrumControllerUtilities;

import static de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator.checkUuidValidity;

public abstract class CerebrumServiceBase<E, R, F> implements CerebrumService<E, R, F>
{

    private final Class<R> repositoryClass;
    private final Class<E> entityClass;
    private final Class <F> foreignKeyExecutorService;
    private static final String PREFIX = "the method, ";

    @Value("#{'${spring.data.rest.base-path}'}")
    private String apiPath;

    protected CerebrumServiceBase(Class<E> entityClass, Class<R> repository, Class<F> foreignKeyExecutorService)
    {
        this.repositoryClass = repository;
        this.entityClass = entityClass;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    protected Method repositoryMethod(String methodName, Class<?>... parameterTypes)
    {
        try {
            return repositoryClass.getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    PREFIX + methodName + "is unknown to the repository: " + repositoryClass.getSimpleName(), e);
        }
    }

    protected Method foreignKeysServiceMethod(String methodName, Class<?>... parameterTypes)
    {
        try {
            return foreignKeyExecutorService.getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    PREFIX + methodName + "is unknown to the foreignKeyExecutorService: " +
                            foreignKeyExecutorService.getSimpleName(), e);
        }
    }

    protected Method entityMethod(String methodName, Class<?>... parameterTypes)
    {
        try {
            return entityClass.getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    PREFIX + methodName + "is unknown to the entity: " + entityClass.getSimpleName(), e);
        }
    }

    protected Object invoke(Method method, Object obj, Object... args)
    {
        try {
            return method.invoke(obj, args);
        } catch (IllegalAccessException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Due to lack of accessibility, this method cannot be invoke", e);
        } catch (InvocationTargetException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "The called method throws an exception, check the cause for details", e);
        }
    }

    protected String getPath(E entity)
    {
        String path;
        switch (entity.getClass().getSimpleName().toLowerCase()) {
            case "software":
                path = "software";
                break;
            case "image":
                path = "images";
                break;
            case "person":
                path = "persons";
                break;
            case "organization":
                path = "organizations";
                break;
            case "marketservice":
                path = "services";
                break;
            case "marketuser":
                path = "users";
                break;
            case "hcarequest":
                path = "hca";
                break;
            default:
                throw new ResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR, "Unknown entity class");
        }
        return path;
    }

    protected Optional<E> findByUuid(String uuid, R repository)
    {
        checkUuidValidity(uuid);
        //noinspection unchecked
        return (Optional<E>) invoke(repositoryMethod("findByUuid", String.class), repository, uuid);
    }

    @Override
    public Page<E> getAllEntities(PageRequest page, R repository)
    {
        //noinspection unchecked
        return (Page<E>) invoke(repositoryMethod("findAll", Pageable.class), repository, page);
    }

    @Override
    public E getEntity(String uuid, R repository)
    {
        return findByUuid(uuid, repository)
                .orElseThrow(() -> new CerebrumEntityNotFoundException(this.entityClass.getName(), uuid));
    }

    @Override
    public E getEntity(String attribute, String value, R repository)
    {
        //noinspection unchecked
        Optional<E> entity = (Optional<E>) invoke(repositoryMethod(
                "findBy" + attribute.substring(0, 1).toUpperCase() + attribute.substring(1), String.class),
                repository, value);

        return entity.orElseThrow(() -> new CerebrumEntityNotFoundException(this.entityClass.getName(), attribute));
    }

    @Override
    public Page<E> getEntities(PageRequest page, String attribute, String value, R repository)
    {
        //noinspection unchecked
        Page<E> entity = (Page<E>) invoke(repositoryMethod(
                "findBy" + attribute.substring(0, 1).toUpperCase() + attribute.substring(1), String.class, PageRequest.class),
                repository, value, page);

        return entity;
    }

    @Override
    public Page<E> searchEntities(PageRequest page, String text, R repository)
    {
        //noinspection unchecked
        Page<E> entity = (Page<E>) invoke(repositoryMethod(
                "search", String.class, PageRequest.class),
                repository, text, page);

        return entity;
    }

    @Override
    public E createEntity(E entity, R repository, F foreignKeyExecutorService)
    {
        invoke(foreignKeysServiceMethod("executeBeforeCreate", Object.class),
                foreignKeyExecutorService, entity);
        //noinspection unchecked
        return (E) invoke(repositoryMethod("save", Object.class), repository, entity);
    }

    @Override
    public ResponseEntity<E> createEntity(E entity, R repository,
                                          F foreignKeyExecutorService,
                                          UriComponentsBuilder uriComponentsBuilder)
    {
        UriComponents uriComponents = uriComponentsBuilder
                .path(apiPath + "/" + getPath(entity) + "/{id}")
                .buildAndExpand(invoke(entityMethod("getUuid"), entity));
        URI location = uriComponents.toUri();
        E createdEntity = createEntity(entity, repository, foreignKeyExecutorService);

        return ResponseEntity.created(location).body(createdEntity);
    }

    @Override
    public ResponseEntity<E> updateEntity(String uuid, E submittedEntity,
                                          R repository, F foreignKeyExecutorService,
                                          UriComponentsBuilder uriComponentsBuilder)
    {
        AtomicBoolean isCreated = new AtomicBoolean(false);
        invoke(entityMethod("setUuid", String.class), submittedEntity, uuid);
        E updatedEntity = findByUuid(uuid, repository).map(retrievedEntity -> {
            try {
                Field f = this.entityClass.getSuperclass().getDeclaredField("createdDate");
                f.setAccessible(true);
                f.set(submittedEntity, f.get(retrievedEntity));
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new ResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR,
                        "Due to lack of accessibility, this method cannot be invoke", e);
            }
            invoke(foreignKeysServiceMethod("executeBeforeUpdate", Object.class, Object.class),
                    foreignKeyExecutorService, retrievedEntity, submittedEntity);
            return createEntity(submittedEntity, repository, foreignKeyExecutorService);
        }).orElseGet(() -> {
            isCreated.set(true);
            invoke(foreignKeysServiceMethod("executeBeforeCreate", Object.class),
                    foreignKeyExecutorService, submittedEntity);
            return createEntity(submittedEntity, repository, foreignKeyExecutorService);
        });

        if (isCreated.get()) {
            UriComponents uriComponents = uriComponentsBuilder
                    .path(apiPath + "/" + getPath(submittedEntity) + "/{id}").buildAndExpand(uuid);
            URI location = uriComponents.toUri();

            return ResponseEntity.created(location).body(updatedEntity);
        }
        return ResponseEntity.ok().body(updatedEntity);
    }

    @Override
    public ResponseEntity<E> partiallyUpdateEntity(String uuid, R repository,
                                                   F foreignKeyExecutorService, JsonPatch patch)
    {
        E partiallyUpdatedEntity = findByUuid(uuid, repository)
                .map(retrievedEntity -> {
                    E patchedEntity = CerebrumControllerUtilities
                            .applyPatch(patch, retrievedEntity, this.entityClass);
                    invoke(foreignKeysServiceMethod("executeBeforeUpdate", Object.class, Object.class),
                            foreignKeyExecutorService, retrievedEntity, patchedEntity);
                    return createEntity(patchedEntity, repository, foreignKeyExecutorService);
                })
                .orElseThrow(()-> new CerebrumEntityNotFoundException(entityClass.getName(), uuid));
        return ResponseEntity.ok().body(partiallyUpdatedEntity);
    }

    @Override
    public ResponseEntity<E>  deleteEntity(String uuid, R repository, F foreignKeyExecutorService)
    {
        checkUuidValidity(uuid);
        //noinspection unchecked
        Optional<E> entity = (Optional<E>)
                invoke(repositoryMethod("deleteByUuid", String.class), repository, uuid);
        if (entity.isPresent()) {
            invoke(foreignKeysServiceMethod("executeAfterDelete", Object.class),
                    foreignKeyExecutorService, entity);
        }
        return ResponseEntity.noContent().build();
    }
}
